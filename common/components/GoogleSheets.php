<?php

namespace common\components;

use yii;
use yii\base\Component;
use Google\Spreadsheet\DefaultServiceRequest;
use Google\Spreadsheet\ServiceRequestFactory;

class GoogleSheets extends Component
{
    public $appName;
    public $pathSecretJson;
    private $client;

    public function insert(string $tableName, array $data)
    {
        $this->initClient();

        try {
            $spreadsheet = (new \Google\Spreadsheet\SpreadsheetService)
                ->getSpreadsheetFeed()
                ->getByTitle($tableName);

            $worksheets = $spreadsheet->getWorksheetFeed()->getEntries();
            $listFeed = $worksheets[0]->getListFeed();
            $listFeed->insert($data);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    private function initClient()
    {
        if ($this->client !== null) {
            return;
        }

        try {
            putenv('GOOGLE_APPLICATION_CREDENTIALS=' . $this->pathSecretJson);
            $this->client = new \Google_Client;
            $this->client->useApplicationDefaultCredentials();
            $this->client->setApplicationName($this->appName);
            $this->client->setScopes(['https://www.googleapis.com/auth/drive', 'https://spreadsheets.google.com/feeds']);
            if ($this->client->isAccessTokenExpired()) {
                $this->client->refreshTokenWithAssertion();
            }
            $accessToken = $this->client->fetchAccessTokenWithAssertion()["access_token"];
            ServiceRequestFactory::setInstance(new DefaultServiceRequest($accessToken));
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
