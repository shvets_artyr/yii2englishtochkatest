<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'googleSheets' => [
            'class' => 'common\components\GoogleSheets',
            'appName' => 'EnglishTochkaTest',
            'pathSecretJson' => yii::getAlias(yii::getAlias('@common/config/google/service_account.json'))
        ],
    ],
];
