<?php

namespace frontend\forms;

use yii;
use yii\base\Model;

class AddRecordLessonForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $whereKnows;

    public function rules()
    {
        return [
            [['name', 'email', 'phone', 'whereKnows'], 'required'],
            ['name', 'string', 'min' => 5, 'max' => 100],
            ['email', 'email'],
            ['name', 'string', 'min' => 5, 'max' => 100],
            ['phone', 'match', 'pattern' => "/^7\s\(\d{3}\)\s\d{3}\-\d{2}\-\d{2}$/", 'message' => 'Неверный номер'],
            ['whereKnows', 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'whereKnows' => 'Откуда вы узнали о нас?',
        ];
    }

    public function getWhere()
    {
        if (isset($this->getArrayKnows()[$this->whereKnows])) {
            return $this->getArrayKnows()[$this->whereKnows];
        }

        return "Не указано";
    }

    public function getArrayKnows()
    {
        return [
            1 => 'Вебинар',
            2 => 'По рекомендации друзей',
            3 => 'Instagram',
            4 => 'Facebook',
            5 => 'Вконтакте',
            6 => 'Youtube',
            7 => 'Telegram',
            8 => 'Письмо на email',
        ];
    }
}
