<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\MaskedInput;
use yii\bootstrap\ActiveForm;

$this->title = 'English Tochka ТЗ 1';
$this->registerJs(new JsExpression("EnglishTochka.initSlickResultGraduate();"), \yii\web\View::POS_END);

?>

<section class="section-intro">
    <div class="dark">
        <div class="content-intro">
            <?= Html::a("Программа-конкурс", '#block-record-lesson', ['class' => 'btn btn-default text-uppercase']); ?>

            <div class="border-block text-uppercase">
                Словарный запас носителя за 1 месяц
            </div>

            <div class="desc-block">
                <ul>
                    <li><i class="fa fa-check" aria-hidden="true"></i> Программа увеличения словарного запаса до уровня
                        носителя языка за 1 месяц
                    </li>
                    <li><i class="fa fa-plus" aria-hidden="true"></i> Призы за наибольшее количество слов</li>
                </ul>
            </div>

            <?= Html::a('Записаться','#block-record-lesson', ['class' => 'btn btn-record text-uppercase']); ?>
        </div>
    </div>
</section>

<section class="section-carrier">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-center text-uppercase">Как вы думаете, сколько слов английского языка используют
                    носители?</h2>


                <div class="carrier-block">
                    <div class="row text-center">
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="carrier-item">
                                <div class="carrier-item-img">
                                    <img src="/img/carrier-1.jpg" class="img-circle" alt="Cinque Terre" width="200" height="200">
                                </div>
                                <div class="carrier-item-footer">
                                    <div class="item-desc">Средний американский подросток</div>
                                    <div class="item-words">400 слов</div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="carrier-item">
                                <div class="carrier-item-img">
                                    <img src="/img/carrier-2.jpg" class="img-circle" alt="Cinque Terre" width="200" height="200">
                                </div>
                                <div class="carrier-item-footer">
                                    <div class="carrier-item-footer">
                                        <div class="item-desc">Средний взрослый <br/>американец</div>
                                        <div class="item-words">3000 слов</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="carrier-item">
                                <div class="carrier-item-img">
                                    <img src="/img/carrier-3.jpg" class="img-circle" alt="Cinque Terre" width="200" height="200">
                                </div>
                                <div class="carrier-item-footer">
                                    <div class="carrier-item-footer">
                                        <div class="item-desc">Средний <br/>иностранец</div>
                                        <div class="item-words">1700 слов</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="block-record-lesson" class="section-lessons">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-center text-uppercase">Запишись на бесплатный вводной урок</h2>

                <?php $form = ActiveForm::begin([
                    'id' => 'record-lesson',
                    'method' => 'post',
                    'options' => [
                        'class' => 'record-lesson',
                    ],
                ]); ?>

                    <?= $form->field($modelForm, 'name', [
                        'template' => '<div class="col-xs-12 col-sm-4">{input}</div>',
                        'inputOptions' => [
                            'class' => 'form-control form-control-lg',
                            'placeholder' => $modelForm->getAttributeLabel('name') . ' *:'
                        ],
                    ]) ?>

                    <?= $form->field($modelForm, 'email', [
                        'template' => '<div class="col-xs-12 col-sm-4">{input}</div>',
                        'inputOptions' => [
                            'class' => 'form-control form-control-lg',
                            'placeholder' => $modelForm->getAttributeLabel('email') . ' *:'
                        ],
                    ]) ?>

                    <?= $form->field($modelForm, 'phone', [
                        'template' => '<div class="col-xs-12 col-sm-4">{input}</div>',
                        'inputOptions' => [
                            'class' => 'form-control form-control-lg',
                            'placeholder' => $modelForm->getAttributeLabel('phone') . ' *:'
                        ],
                    ])->widget(MaskedInput::className(), [
                        'mask' => "7 (999) 999-99-99",
                        'options' => [
                            'class' => 'form-control form-control-lg',
                            'placeholder' => '7 (___) ___-__-__',
                        ],
                    ])->label('Введіть номер телефону клієнта') ?>

                    <?= $form->field($modelForm, 'whereKnows', [
                        'template' => '<div class="col-xs-12 col-sm-4 col-sm-offset-4 mt-15">{label}{input}</div>',
                        'inputOptions' => [
                            'class' => 'form-control form-control-lg bg-light',
                        ],
                    ])->dropDownList($modelForm->getArrayKnows()) ?>

                    <div class="col-xs-12 col-sm-4 col-sm-offset-4">
                        <?= Html::submitButton('Записаться', ['class' => 'btn btn-record text-uppercase w-100 mt-15']) ?>
                    </div>

                <?php $form::end(); ?>

            </div>
        </div>
    </div>
</section>

<section class="section-result">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-center text-uppercase">Ваши результаты в конце программы-конкурса</h2>

            </div>
        </div>
    </div>

    <div class="bg-result"></div>
</section>

<section class="section-try">
    <div class="hr"></div>

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-center text-uppercase">Бесплатно пропробуйте запоминать <br/>слова уже сейчас</h2>

                <div class="col-xs-12 col-sm-4 col-sm-offset-4 text-center">
                    <?= Html::a('Попробовать', '#block-record-lesson', ['class' => 'btn btn-record text-uppercase w-100 mt-15']); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="hr"></div>
</section>

<section class="section-choise">
    <div class="choise-bg"></div>
</section>

<section class="section-result-graduate">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                <div class="graduate-block-wrap">
                    <div class="slick-slider-graduate">
                        <div class="slick-slide">
                            <div class="col-xs-12 col-sm-12 col-md-5">
                                <div class="graduate-block-img">
                                    <img src="/img/graduate-1.jpg">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-7">
                                <div class="graduate-block-info">
                                    <h2 class="text-uppercase">Результаты выпускников прошлого сезона</h2>
                                    <div class="small">(средний участник запоминает 1157 слов)</div>

                                    <div class="graduate-info">
                                        <div class="name">Юрчук Алексей</div>
                                        <div class="city">Санкт-Петербург</div>

                                        <div class="desc">
                                            + 1157 слов <br/>
                                            +  уровня в словарном запасе <br/>
                                            + возраст как использует <br/>
                                            + отзыв <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="slick-slide">
                            <div class="col-xs-12 col-sm-12 col-md-5">
                                <div class="graduate-block-img">
                                    <img src="/img/graduate-1.jpg">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-7">
                                <div class="graduate-block-info">
                                    <h2 class="text-uppercase">Результаты выпускников прошлого сезона 2</h2>
                                    <div class="small">(средний участник запоминает 1157 слов 2)</div>

                                    <div class="graduate-info">
                                        <div class="name">Юрчук Алексей 2</div>
                                        <div class="city">Санкт-Петербург</div>

                                        <div class="desc">
                                            + 1157 слов <br/>
                                            +  уровня в словарном запасе <br/>
                                            + возраст как использует <br/>
                                            + отзыв <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<section class="section-pay">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                <div class="col-xs-12 col-sm-6">
                    <div class="pay-block right">
                        <div class="title">На одного</div>
                        <div class="info">
                            <div class="desc">Без конкурса</div>
                            <img src="/img/p-one.jpg">

                            <div class="price-old">
                                6 <span class="up">900</span> <span class="currency">руб.</span>
                                <div class="line"></div>
                            </div>

                            <div class="price-new">
                                3 <span class="up">900</span> <span class="currency">руб.</span>
                            </div>

                            <?= Html::a('Купить', '#block-record-lesson', ['class' => 'btn btn-record text-uppercase w-100 mt-15']); ?>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6">
                    <div class="pay-block left">
                        <div class="title">На одного</div>
                        <div class="info">
                            <div class="desc competition">С конкурсом</div>
                            <img src="/img/p-one.jpg">

                            <div class="price-old">
                                8 <span class="up">900</span> <span class="currency">руб.</span>
                                <div class="line"></div>
                            </div>

                            <div class="price-new">
                                4 <span class="up">900</span> <span class="currency">руб.</span>
                            </div>

                            <?= Html::a('Купить', '#block-record-lesson', ['class' => 'btn btn-record text-uppercase w-100 mt-15']); ?>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6">
                    <div class="pay-block right">
                        <div class="title green">На двоих</div>
                        <div class="info">
                            <div class="desc">Без конкурса</div>
                            <img src="/img/p-two.jpg">

                            <div class="price-old">
                                13 <span class="up">900</span> <span class="currency">руб.</span>
                                <div class="line"></div>
                            </div>

                            <div class="price-new">
                                6 <span class="up">900</span> <span class="currency">руб.</span>
                            </div>

                            <?= Html::a('Купить', '#block-record-lesson', ['class' => 'btn btn-record text-uppercase w-100 mt-15']); ?>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6">
                    <div class="pay-block left">
                        <div class="title green">На двоих</div>
                        <div class="info">
                            <div class="desc competition">С конкурсом</div>
                            <img src="/img/p-two.jpg">

                            <div class="price-old">
                                17 <span class="up">900</span> <span class="currency">руб.</span>
                                <div class="line"></div>
                            </div>

                            <div class="price-new">
                                8 <span class="up">900</span> <span class="currency">руб.</span>
                            </div>

                            <?= Html::a('Купить', '#block-record-lesson', ['class' => 'btn btn-record text-uppercase w-100 mt-15']); ?>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>

<section class="section-footer-try">
    <div class="hr"></div>

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-center text-uppercase">Бесплатно пропробуйте запоминать <br/>слова уже сейчас</h2>

                <div class="col-xs-12 col-sm-4 col-sm-offset-4 text-center">
                    <?= Html::a('Попробовать', '#block-record-lesson', ['class' => 'btn btn-record text-uppercase w-100 mt-15']); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="hr"></div>
</section>
