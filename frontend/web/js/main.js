var EnglishTochka = {
    init : function() {
        this.config();
        this.events();
    },

    config : function() {
        this.config = {
            window : $(window),
            document : $(document),
        };
    },

    events : function() {
        var self = this;

        self.config.document.ready(function() {

        });

        self.config.window.on('load', function() {

        });
    },

    initSlickResultGraduate : function() {
        $('.slick-slider-graduate').slick({
            infinite: false,
            dots: false,
            arrows: true,
            fade: true,
            slidesToShow: 1,
            slidesToScroll: 1
        });
    }
};

EnglishTochka.init();