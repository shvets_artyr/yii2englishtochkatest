<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class MainAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'libs/slick-1.8.0/slick/slick.css',
        'libs/slick-1.8.0/slick/slick-theme.css',
        'css/font-awesome.min.css',
        'css/main.css',
        'css/slick.css'
    ];
    public $js = [
        'libs/slick-1.8.0/slick/slick.js',
        'js/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
