<?php

namespace frontend\controllers;

use yii;
use yii\web\Controller;
use frontend\forms\AddRecordLessonForm;
use Google\Spreadsheet\DefaultServiceRequest;
use Google\Spreadsheet\ServiceRequestFactory;

class TaskOneController extends Controller
{
    public $layout = 'task-one';

    public function actionIndex()
    {
        $modelForm = new AddRecordLessonForm();

        if ($modelForm->load(yii::$app->request->post()) && $modelForm->validate()) {
            yii::$app->googleSheets->insert('EnglishTochkaTest', [
                'name' => $modelForm->name,
                'email' => $modelForm->email,
                'phone' => $modelForm->phone,
                'where' => $modelForm->getWhere(),
                'date' => date("M d Y H:i:s")
            ]);

            $this->redirect('https://docs.google.com/spreadsheets/d/1cVeXwUnRYgKTzuw7ae9pfjg1LqcbJmAgzaiBhbulNfo/edit#gid=0');
        }

        return $this->render('index', [
            'modelForm' => $modelForm
        ]);
    }
}
